import functools, operator

def factorial(ind):
    return functools.reduce(operator.mul, range(1, ind))

print(factorial(40)/factorial(20))