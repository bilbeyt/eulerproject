def generate_value(given):
    if given %2 == 0:
        return given//2
    else:
        return 3*given+1

chains = []
start_num = 1

while start_num < 1000000:
    count = 0
    chain_num = start_num
    while(chain_num != 1):
        chain_num = generate_value(chain_num)
        count += 1
    chains.append([start_num, count])
    print(start_num, count)
    start_num += 1

chains.sort(key=lambda x: x[1])
print(chains[-1][0])