def find_total(given_limit):
    nums = [3, 5]
    dividers = []
    for num in nums:
        count = 1
        while(given_limit > num*count):
            if num*count not in dividers:
                dividers.append(num*count)
            count += 1
    print(dividers)
    return sum(dividers)

print(find_total(1000))
            