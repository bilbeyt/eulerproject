primes = []


def find_primes(limit):
    num = 2
    while(num < limit):
        check = True
        if num not in primes:
            for j in primes:
                if num % j == 0:
                    check = False
                    break
            if check:
                print(num)
                primes.append(num)
        num += 1
    return sum(primes)


print(find_primes(2000000))