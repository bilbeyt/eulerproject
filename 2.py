evens = []


def fibonacci(i):
    if i == 1 or i == 2:
        return 1
    return fibonacci(i-1) + fibonacci(i-2)


count = 1
while(1):
    val = fibonacci(count)
    if val >= 4000000:
        break
    if val % 2 == 0:
        evens.append(val)
    count += 1
print(sum(evens))
