primes = []

def prime(num):
    for i in range(2, num):
        if num%i == 0 and i not in primes:
            primes.append(i)
            num = num//i
        if num == 1:
            break
    return max(primes)

print(prime(600851475143))